# The operator rem(a, b) returns the remainder after dividing a by b. Write a
# function that takes a single integer (n) and calls the function in the
# previous exercise, passing it rem(n,3), rem(n,5), and n. Call it seven times
# with the arguments 10, 11, 12, and so on. You should get "Buzz, 11, Fizz, 13,
# 14, FizzBuzz, 16."
#
# (Yes, it’s a FizzBuzz solution with no conditional logic.)

fizzbuzz = fn
  (0, 0, _) -> "FizzBuzz"
  (0, _, _) -> "Fizz"
  (_, 0, _) -> "Buzz"
  (_, _, c) -> c
end

exercise = fn
  n -> fizzbuzz.(rem(n, 3), rem(n, 5), n)
end

IO.puts exercise.(10)
IO.puts exercise.(11)
IO.puts exercise.(12)
IO.puts exercise.(13)
IO.puts exercise.(14)
IO.puts exercise.(15)
IO.puts exercise.(16)
