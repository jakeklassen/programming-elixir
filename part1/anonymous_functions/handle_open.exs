# Lots to digest here

# Anonymous Function
# Handle the result of passing in File.open(...)
handle_open = fn
  # Function body clauses.
  # Notice the string interpolation using "#{...}"

  # Successful File.open
  {:ok, file} -> "Read Data: #{IO.read(file, :line)}"

  # Unsuccessful File.open
  # Note the use of :file.format_error()
  # The atom :file refers to the underlying Erlang :file module and can be
  # easily used within Elixir to save work.
  {_,  error} -> "Error: #{:file.format_error(error)}"
end

# Use handle_open

# Note the use of File.open - this is the Elixir File module
IO.puts handle_open.(File.open("part1/anonymous_functions/data/file.md"))
IO.puts handle_open.(File.open("does_not_exist.md"))
