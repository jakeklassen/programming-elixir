# Use the &... notation to rewrite the following:
#
# Enum.map [1, 2, 3, 4], fn -> x + 2 end
# Enum.each [1, 2, 3, 4], fn -> IO.inspect x end

IO.puts Enum.map  [1,2,3,4], &(&1 + 2)
IO.puts Enum.each [1,2,3,4], &IO.inspect/1
