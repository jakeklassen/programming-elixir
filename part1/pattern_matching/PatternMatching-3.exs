# If you assume the variable a initially contains the value 2, which of the
# following will match?

# ^a means retain value

# Fail
[a, b, a] = [1, 2, 3]

# Fail
[a, b, a] = [1, 1, 2]

# Match
a = 1

# Match
^a = 2

# Fail
^a = 1

# Fail
^a = 2 - a
