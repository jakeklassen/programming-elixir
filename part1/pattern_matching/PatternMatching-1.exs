# Exercise
# Which of the following match?

# Match
a = [1, 2, 3]

# Match
a = 4

# Match
4 = a

# Fail - No lhs match for 3
[a, b] = [ 1, 2, 3 ]

# Match
a = [ [ 1, 2, 3 ] ]

# Match
[a] = [ [ 1, 2, 3 ] ]

# Fail - No lhs match for [2,3]
[[a]] = [ [ 1, 2, 3 ] ]
