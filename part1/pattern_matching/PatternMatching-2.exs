# What of the following will match?

# Fail
# a matchs 1 in rhs
# a tries to match 3 on rhs with value 1 and fails
[a, b, a] = [1, 2, 3]

# Fail
# a matchs 1 in rhs
# a tries to match 2 on rhs with value 1 and fails
[a, b, a] = [1, 1, 2]

# Match
# a takes on value 1
# a matches 1 again on rhs
[a, b, a] = [1, 2, 1]
