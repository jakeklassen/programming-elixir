## Notes

`.esx` extension is for code we can/want to run directly from a source file.
You can think of the `s` as `script`.

`.ex` extension is for code we'll compile and use later.
